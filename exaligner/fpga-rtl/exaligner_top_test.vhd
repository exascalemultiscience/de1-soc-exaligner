library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.constants.all;

entity exaligner_top_test is
end exaligner_top_test;

architecture exaligner_top_test_architecture of exaligner_top_test is
  constant CLOCK_FREQUENCY :    integer := 40000000;
  constant CLOCK_PERIOD :       time := 1 sec / CLOCK_FREQUENCY;

  signal g_clk :                std_logic := '0';
  signal g_reset :              std_logic := '1';

  signal test_chipselect :      std_logic := '1';
  signal test_address :         std_logic_vector (1 downto 0);
  signal test_read :            std_logic := '0';
  signal test_write :           std_logic := '0';
  signal test_writedata :       std_logic_vector (AVALON_MM_DATA_WIDTH-1 downto 0);
  signal test_readdata :        std_logic_vector (AVALON_MM_DATA_WIDTH-1 downto 0);
begin
  exalignerTop: entity work.exaligner_top
  port map (
    clk => g_clk,
    reset => g_reset,
    chipselect => test_chipselect,
    address => test_address,
    read => test_read,
    write => test_write,
    writedata => test_writedata,
    readdata => test_readdata
  );
  g_clk <= not g_clk after CLOCK_PERIOD / 2;

  testProcess: process is
    constant testShortRead: std_logic_vector(AVALON_MM_DATA_WIDTH-1 downto 0) := "00000111000101001001001010110000";
    constant testShortRead2: std_logic_vector(AVALON_MM_DATA_WIDTH-1 downto 0) := "11000101111001111010110010111101";
    constant testShortRead3: std_logic_vector(AVALON_MM_DATA_WIDTH-1 downto 0) := "01101000000111000101001001001001";
    constant testShortRead4: std_logic_vector(AVALON_MM_DATA_WIDTH-1 downto 0) := "00111001001011101101100110011000";
    constant testReference: std_logic_vector(4*AVALON_MM_DATA_WIDTH-1 downto 0) :=
      "11101011000001101011100110101000010110011011011001100110000100011110000011011001001110000011110001011110011110101100101111010110";
    variable counter:       natural;
  begin
    wait until (rising_edge(g_clk));
    test_write <= '0';
    g_reset <= '0';
    wait until (rising_edge(g_clk));
    wait until (rising_edge(g_clk));
    wait until (rising_edge(g_clk));
    
    for i in 0 to N_PRINCIPAL_AGENT_NUCLEOTIDES-1 loop
      test_write <= '1';
      test_address <= AVALON_MM_SLAVE_WRITE_ADDRESS;
      test_writedata <= (AVALON_MM_DATA_WIDTH-1 downto 2 => '0') & testReference(2*i+1 downto 2*i);
      wait until (rising_edge(g_clk));
      wait until (rising_edge(g_clk));
      test_write <= '0';
      wait until (rising_edge(g_clk));
    end loop;
    
    wait until (rising_edge(g_clk));
    wait until (rising_edge(g_clk));
    wait until (rising_edge(g_clk));
    
    test_read <= '1';
    test_address <= AVALON_MM_SLAVE_STATUS_ADDRESS;
    wait until (rising_edge(g_clk));
    wait until (rising_edge(g_clk));
    test_read <= '0';
    wait until (rising_edge(g_clk));
    while (test_readdata(1) = '0') loop -- while not ready
      test_read <= '1';
      test_address <= AVALON_MM_SLAVE_STATUS_ADDRESS;
      wait until (rising_edge(g_clk));
      wait until (rising_edge(g_clk));
      test_read <= '0';
      wait until (rising_edge(g_clk));
    end loop;
    
    test_write <= '1';
    test_address <= AVALON_MM_SLAVE_WRITE_ADDRESS;
    test_writedata <= std_logic_vector(to_unsigned(16, 32));
    wait until (rising_edge(g_clk));
    wait until (rising_edge(g_clk));
    for i in 0 to 15 loop
      test_write <= '1';
      test_address <= AVALON_MM_SLAVE_WRITE_ADDRESS;
      test_writedata <= (AVALON_MM_DATA_WIDTH-1 downto 2 => '0') & testShortRead(2*i+1 downto 2*i);
      wait until (rising_edge(g_clk));
      wait until (rising_edge(g_clk));
      test_write <= '0';
      wait until (rising_edge(g_clk));
    end loop;
    
    test_read <= '1';
    test_address <= AVALON_MM_SLAVE_STATUS_ADDRESS;
    wait until (rising_edge(g_clk));
    wait until (rising_edge(g_clk));
    test_read <= '0';
    wait until (rising_edge(g_clk));
    while (test_readdata(1) = '0') loop -- while not ready
      test_read <= '1';
      test_address <= AVALON_MM_SLAVE_STATUS_ADDRESS;
      wait until (rising_edge(g_clk));
      wait until (rising_edge(g_clk));
      test_read <= '0';
      wait until (rising_edge(g_clk));
    end loop;
    
    test_write <= '1';
    test_address <= AVALON_MM_SLAVE_WRITE_ADDRESS;
    test_writedata <= std_logic_vector(to_unsigned(16, 32));
    wait until (rising_edge(g_clk));
    wait until (rising_edge(g_clk));
    for i in 0 to 15 loop
      test_write <= '1';
      test_address <= AVALON_MM_SLAVE_WRITE_ADDRESS;
      test_writedata <= (AVALON_MM_DATA_WIDTH-1 downto 2 => '0') & testShortRead2(2*i+1 downto 2*i);
      wait until (rising_edge(g_clk));
      wait until (rising_edge(g_clk));
      test_write <= '0';
      wait until (rising_edge(g_clk));
    end loop;

    test_read <= '1';
    test_address <= AVALON_MM_SLAVE_STATUS_ADDRESS;
    wait until (rising_edge(g_clk));
    wait until (rising_edge(g_clk));
    test_read <= '0';
    wait until (rising_edge(g_clk));
    while (test_readdata(1) = '0') loop -- while not ready
      test_read <= '1';
      test_address <= AVALON_MM_SLAVE_STATUS_ADDRESS;
      wait until (rising_edge(g_clk));
      wait until (rising_edge(g_clk));
      test_read <= '0';
      wait until (rising_edge(g_clk));
    end loop;
    
    test_write <= '1';
    test_address <= AVALON_MM_SLAVE_WRITE_ADDRESS;
    test_writedata <= std_logic_vector(to_unsigned(16, 32));
    wait until (rising_edge(g_clk));
    wait until (rising_edge(g_clk));
    for i in 0 to 15 loop
      test_write <= '1';
      test_address <= AVALON_MM_SLAVE_WRITE_ADDRESS;
      test_writedata <= (AVALON_MM_DATA_WIDTH-1 downto 2 => '0') & testShortRead3(2*i+1 downto 2*i);
      wait until (rising_edge(g_clk));
      wait until (rising_edge(g_clk));
      test_write <= '0';
      wait until (rising_edge(g_clk));
    end loop;
    
    test_read <= '1';
    test_address <= AVALON_MM_SLAVE_STATUS_ADDRESS;
    wait until (rising_edge(g_clk));
    wait until (rising_edge(g_clk));
    test_read <= '0';
    wait until (rising_edge(g_clk));
    while (test_readdata(1) = '0') loop -- while not ready
      test_read <= '1';
      test_address <= AVALON_MM_SLAVE_STATUS_ADDRESS;
      wait until (rising_edge(g_clk));
      wait until (rising_edge(g_clk));
      test_read <= '0';
      wait until (rising_edge(g_clk));
    end loop;
    
    test_write <= '1';
    test_address <= AVALON_MM_SLAVE_WRITE_ADDRESS;
    test_writedata <= std_logic_vector(to_unsigned(16, 32));
    wait until (rising_edge(g_clk));
    wait until (rising_edge(g_clk));
    for i in 0 to 15 loop
      test_write <= '1';
      test_address <= AVALON_MM_SLAVE_WRITE_ADDRESS;
      test_writedata <= (AVALON_MM_DATA_WIDTH-1 downto 2 => '0') & testShortRead4(2*i+1 downto 2*i);
      wait until (rising_edge(g_clk));
      wait until (rising_edge(g_clk));
      test_write <= '0';
      wait until (rising_edge(g_clk));
    end loop;
    
    wait for 2000 ns;
    
    test_read <= '1';
    test_address <= AVALON_MM_SLAVE_STATUS_ADDRESS;
    wait until (rising_edge(g_clk));
    wait until (rising_edge(g_clk));
    test_read <= '0';
    wait until (rising_edge(g_clk));
    while (test_readdata(0) = '0') loop -- while not empty
      test_read <= '1';
      test_address <= AVALON_MM_SLAVE_READ_ADDRESS;
      wait until (rising_edge(g_clk));
      wait until (rising_edge(g_clk));
      test_read <= '0';
      wait until (rising_edge(g_clk));
      
      test_read <= '1';
      test_address <= AVALON_MM_SLAVE_STATUS_ADDRESS;
      wait until (rising_edge(g_clk));
      wait until (rising_edge(g_clk));
      test_read <= '0';
      wait until (rising_edge(g_clk));
    end loop;
    
    wait until (rising_edge(g_clk));
    wait until (rising_edge(g_clk));
    wait until (rising_edge(g_clk));
    
  end process testProcess;

end architecture exaligner_top_test_architecture;