library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.math_real.all;

package constants is
  constant MAX_N_SHORT_READ_NUCLEOTIDES:     natural := 256;
  constant N_PRINCIPAL_AGENT_NUCLEOTIDES:    natural := 1024;
  constant MAX_ERROR_COUNT:                  natural := 3; -- 0 <= MAX_ERROR_COUNT <= MAX_N_SHORT_READ_NUCLEOTIDES
  constant ERROR_COUNT_RESET_VALUE:          integer := -MAX_ERROR_COUNT - 1;
  constant ERROR_COUNT_WIDTH:                natural := natural(ceil(log2(real(MAX_N_SHORT_READ_NUCLEOTIDES)))) + 1;
  constant REFERENCE_OFFSET_WIDTH:           natural := natural(ceil(log2(real(N_PRINCIPAL_AGENT_NUCLEOTIDES))));
  constant RESULT_WIDTH:                     natural := ERROR_COUNT_WIDTH + REFERENCE_OFFSET_WIDTH;
  constant FIFO_BUFFER_LENGTH:               natural := 16;
  constant FIFO_ADDRESS_WIDTH:               natural := natural(ceil(log2(real(FIFO_BUFFER_LENGTH+1))));
  constant AVALON_MM_DATA_WIDTH:             natural := 32;
  constant AVALON_MM_SLAVE_WRITE_ADDRESS:    std_logic_vector(1 downto 0) := "00";
  constant AVALON_MM_SLAVE_RESET_ADDRESS:    std_logic_vector(1 downto 0) := "01";
  constant AVALON_MM_SLAVE_READ_ADDRESS:     std_logic_vector(1 downto 0) := "10";
  constant AVALON_MM_SLAVE_STATUS_ADDRESS:   std_logic_vector(1 downto 0) := "11";
end;