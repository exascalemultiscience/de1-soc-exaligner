library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.constants.all;
use work.array_types.all;

entity exaligner_top is
  port (
    -- inputs:
    clk :           in std_logic;
    reset :         in std_logic;
    chipselect :    in std_logic;
    address :       in std_logic_vector (1 downto 0);
    read :          in std_logic;
    write :         in std_logic;
    writedata :     in std_logic_vector (AVALON_MM_DATA_WIDTH-1 downto 0);

    -- outputs:
    readdata :      out std_logic_vector (AVALON_MM_DATA_WIDTH-1 downto 0)
  );
end entity exaligner_top;

architecture exaligner_top_architecture of exaligner_top is
  signal g_reset:                                  std_logic;

  signal reset_error_count:                        std_logic;
  signal shift:                                    std_logic;
  signal short_read_nucleotide:                    std_logic_vector(1 downto 0);
  signal reference_nucleotide_source:              std_logic_vector(1 downto 0);
  signal reference_nucleotide_sink:                std_logic_vector(1 downto 0);
  
  signal serializer_init:                          std_logic;
  signal serializer_error_count_array:             ErrorCountArray;
  signal serializer_error_count:                   std_logic_vector(ERROR_COUNT_WIDTH-1 downto 0);
  signal serializer_match_found_array:             std_logic_vector(N_PRINCIPAL_AGENT_NUCLEOTIDES-1 downto 0);
  signal serializer_reference_offset:              std_logic_vector(REFERENCE_OFFSET_WIDTH-1 downto 0);
  signal serializer_ready:                         std_logic;
  
  signal read_started:                             std_logic;
  signal fifo_almost_full:                         std_logic;
  signal fifo_write_enabled:                       std_logic;
  signal fifo_read_enabled:                        std_logic;
  signal fifo_empty:                               std_logic;
  signal fifo_data_in:                             std_logic_vector(RESULT_WIDTH-1 downto 0);
  signal fifo_data_out:                            std_logic_vector(RESULT_WIDTH-1 downto 0);
  
  signal state_init:                               std_logic;
  signal init_done:                                std_logic;
  type InitPhaseType is (INIT_PHASE_START, INIT_PHASE_WAIT, INIT_PHASE_SHIFT, INIT_PHASE_DONE);
  signal init_phase:                               InitPhaseType;
  signal next_init_phase:                          InitPhaseType;
  signal init_counter:                             natural;
  signal next_init_counter:                        natural;
  signal init_shift:                               std_logic;
  signal init_reference_nucleotide_source:         std_logic_vector(1 downto 0);
  
  signal state_match:                              std_logic;
  type MatchPhaseType is (MATCH_PHASE_START, MATCH_PHASE_SIZE_WAIT, MATCH_PHASE_SIZE_DONE, MATCH_PHASE_WAIT, MATCH_PHASE_SHIFT, MATCH_PHASE_MATCH_DONE, MATCH_PHASE_SERIALIZE_INIT, MATCH_PHASE_SERIALIZE, MATCH_PHASE_DONE);
  signal match_phase:                              MatchPhaseType;
  signal next_match_phase:                         MatchPhaseType;
  signal match_counter:                            natural;
  signal next_match_counter:                       natural;
  signal match_short_read_size:                    natural;
  signal next_match_short_read_size:               natural;
  signal match_shift:                              std_logic;
  signal match_status_ready:                       std_logic;
  
begin

  principalAgent: entity work.principal_agent
  port map (
    CLK => clk,
    RESET => g_reset,
    RESET_ERROR_COUNT => reset_error_count,
    SHIFT => shift,
    SHORT_READ_NUCLEOTIDE => short_read_nucleotide,
    REFERENCE_NUCLEOTIDE_IN => reference_nucleotide_source,
    REFERENCE_NUCLEOTIDE_OUT => reference_nucleotide_sink,
    MATCH_FOUND_ARRAY => serializer_match_found_array,
    ERROR_COUNT_ARRAY => serializer_error_count_array
  );
  serializer: entity work.serializer
  port map (
    CLK => clk,
    RESET => g_reset,
    INIT => serializer_init,
    MATCH_FOUND_ARRAY => serializer_match_found_array,
    ERROR_COUNT_ARRAY => serializer_error_count_array,
    ERROR_COUNT => serializer_error_count,
    REFERENCE_OFFSET => serializer_reference_offset,
    READY => serializer_ready
  );
  fifo: entity work.fifo
  generic map (
    ADDR_W => FIFO_ADDRESS_WIDTH,
    DATA_W => RESULT_WIDTH,
    BUFF_L => FIFO_BUFFER_LENGTH,
    ALMST_F => 3,
    ALMST_E => 3
  )
  port map (
    clk => clk,
    reset => g_reset,
    rd_en => fifo_read_enabled,
    wr_en => fifo_write_enabled,
    data_in => fifo_data_in,
    data_out => fifo_data_out,
    data_count => open,
    empty => fifo_empty,
    almst_empty => open,
    almst_full => fifo_almost_full,
    err => open,
    full => open
  );
  
  g_reset <= '1' when address = AVALON_MM_SLAVE_RESET_ADDRESS and writedata(0) = '1' else
             reset;
  
  shift <= init_shift  when state_init = '1' else
           match_shift when state_match = '1' else
           '0';
  
  reference_nucleotide_source <= init_reference_nucleotide_source when state_init = '1' else
                                 reference_nucleotide_sink        when state_match = '1' else
                                 "00";
  
  fifo_data_in <= serializer_error_count & serializer_reference_offset;
  
  match_status_ready <= '1' when state_match = '1' and (match_phase = MATCH_PHASE_START or match_phase = MATCH_PHASE_SIZE_WAIT) else
                        '0';
  
  readdata <= (AVALON_MM_DATA_WIDTH-1 downto RESULT_WIDTH => '0') & fifo_data_out        when address = AVALON_MM_SLAVE_READ_ADDRESS else
              (AVALON_MM_DATA_WIDTH-2 downto 1 => '0') & match_status_ready & fifo_empty when address = AVALON_MM_SLAVE_STATUS_ADDRESS else
              (others => '1');
  
  fifo_read_enabled <= '1' when (read = '1' and address = AVALON_MM_SLAVE_READ_ADDRESS) and read_started = '0' else
                       '0';
  
  -- read
  process (g_reset, clk)
  begin
    if (g_reset = '1') then
      read_started <= '0';
    elsif (rising_edge(clk)) then
      if (read = '1') then
        read_started <= '1';
      end if;
      if (read_started = '1') then
        read_started <= '0';
      end if;
    end if;
  end process;
  
  -- state transitions
  process (clk, g_reset, next_init_counter, next_init_phase, init_done)
  begin 
    if (g_reset = '1') then
      state_init <= '1';
      state_match <= '0';
      init_phase <= INIT_PHASE_START;
      init_counter <= 0;
      match_phase <= MATCH_PHASE_START;
      match_counter <= 0;
      match_short_read_size <= 0;
    elsif (rising_edge(clk)) then
      if (state_init <= '1') then
        init_phase <= next_init_phase;
        init_counter <= next_init_counter;
      end if;
      
      if (init_done = '1') then
        state_init <= '0';
        state_match <= '1';
      end if;
      
      if (state_match <= '1') then
        match_phase <= next_match_phase;
        match_counter <= next_match_counter;
        match_short_read_size <= next_match_short_read_size;
      end if;
    end if;
  end process;
  
  -- INIT
  process (g_reset, state_init, init_phase, init_counter, chipselect, write, writedata, address)
  begin
    if (g_reset = '1') then
      next_init_phase <= INIT_PHASE_START;
      next_init_counter <= 0;
      init_done <= '0';
      init_shift <= '0';
      init_reference_nucleotide_source <= "00";
    elsif (state_init = '1') then
      next_init_phase <= init_phase;
      next_init_counter <= init_counter;
      init_done <= '0';
      init_shift <= '0';
      init_reference_nucleotide_source <= "00";
      if (init_phase = INIT_PHASE_START) then
        next_init_phase <= INIT_PHASE_WAIT;
        next_init_counter <= 0;
      elsif (init_phase = INIT_PHASE_WAIT) then
        if (chipselect = '1' and write = '1' and address = AVALON_MM_SLAVE_WRITE_ADDRESS) then
          init_shift <= '1';
          next_init_phase <= INIT_PHASE_SHIFT;
        end if;
      elsif (init_phase = INIT_PHASE_SHIFT) then
        init_reference_nucleotide_source <= writedata(1 downto 0);
        if (init_counter = N_PRINCIPAL_AGENT_NUCLEOTIDES - 1) then
          next_init_phase <= INIT_PHASE_DONE;
        else
          next_init_phase <= INIT_PHASE_WAIT;
          next_init_counter <= init_counter + 1;
        end if;
      elsif (init_phase = INIT_PHASE_DONE) then
        init_done <= '1';
      end if;
    else
      next_init_phase <= INIT_PHASE_START;
      next_init_counter <= 0;
      init_done <= '0';
      init_shift <= '0';
      init_reference_nucleotide_source <= "00";
    end if;
  end process;
  
  -- MATCH
  process (g_reset, state_match, match_phase, match_counter, match_short_read_size, serializer_ready, chipselect, write, writedata, address)
  begin
    if (g_reset = '1') then
      next_match_phase <= MATCH_PHASE_START;
      next_match_counter <= 0;
      next_match_short_read_size <= 0;
      match_shift <= '0';
      reset_error_count <= '0';
      short_read_nucleotide <= "00";
      serializer_init <= '0';
      fifo_write_enabled <= '0';
    elsif (state_match = '1') then
      next_match_phase <= match_phase;
      next_match_counter <= match_counter;
      next_match_short_read_size <= match_short_read_size;
      match_shift <= '0';
      reset_error_count <= '0';
      short_read_nucleotide <= "00";
      serializer_init <= '0';
      fifo_write_enabled <= '0';
      if (match_phase = MATCH_PHASE_START) then
        reset_error_count <= '1';
        next_match_phase <= MATCH_PHASE_SIZE_WAIT;
        next_match_counter <= 0;
      elsif (match_phase = MATCH_PHASE_SIZE_WAIT) then
        if (chipselect = '1' and write = '1' and address = AVALON_MM_SLAVE_WRITE_ADDRESS) then
          next_match_short_read_size <= to_integer(unsigned(writedata)); -- TODO tesztelni
          next_match_phase <= MATCH_PHASE_SIZE_DONE;
        end if;
      elsif (match_phase = MATCH_PHASE_SIZE_DONE) then
        next_match_phase <= MATCH_PHASE_WAIT;
      elsif (match_phase = MATCH_PHASE_WAIT) then
        if (chipselect = '1' and write = '1' and address = AVALON_MM_SLAVE_WRITE_ADDRESS) then
          match_shift <= '1';
          next_match_phase <= MATCH_PHASE_SHIFT;
        end if;
      elsif (match_phase = MATCH_PHASE_SHIFT) then
        short_read_nucleotide <= writedata(1 downto 0);
        if (match_counter = match_short_read_size - 1) then
          next_match_phase <= MATCH_PHASE_MATCH_DONE;
        else
          next_match_phase <= MATCH_PHASE_WAIT;
          next_match_counter <= match_counter + 1;
        end if;
      elsif (match_phase = MATCH_PHASE_MATCH_DONE) then
        serializer_init <= '1';
        next_match_phase <= MATCH_PHASE_SERIALIZE_INIT;
      elsif (match_phase = MATCH_PHASE_SERIALIZE_INIT) then
        next_match_phase <= MATCH_PHASE_SERIALIZE;
      elsif (match_phase = MATCH_PHASE_SERIALIZE) then
        if (serializer_ready = '1') then
          next_match_phase <= MATCH_PHASE_DONE;
        else
          fifo_write_enabled <= '1';
        end if;
      elsif (match_phase = MATCH_PHASE_DONE) then
        next_match_phase <= MATCH_PHASE_START;
      end if;
    else
      next_match_phase <= MATCH_PHASE_START;
      next_match_counter <= 0;
      next_match_short_read_size <= 0;
      match_shift <= '0';
      reset_error_count <= '0';
      short_read_nucleotide <= "00";
      serializer_init <= '0';
      fifo_write_enabled <= '0';
    end if;
  end process;
  
end exaligner_top_architecture;