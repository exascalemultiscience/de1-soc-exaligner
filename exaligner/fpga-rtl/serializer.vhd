library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.constants.all;
use work.array_types.all;

entity serializer is
  port (
    CLK:               in std_logic;
    RESET:             in std_logic;
    
    INIT:              in std_logic;
    MATCH_FOUND_ARRAY: in std_logic_vector(N_PRINCIPAL_AGENT_NUCLEOTIDES-1 downto 0);
    ERROR_COUNT_ARRAY: in ErrorCountArray;
    ERROR_COUNT:       out std_logic_vector(ERROR_COUNT_WIDTH-1 downto 0);
    REFERENCE_OFFSET:  out std_logic_vector(REFERENCE_OFFSET_WIDTH-1 downto 0);
    READY:             out std_logic
  );
end serializer;

architecture serializer_architecture of serializer is
  signal match_found_array_internal: std_logic_vector(N_PRINCIPAL_AGENT_NUCLEOTIDES-1 downto 0);
begin
  process (CLK, RESET) is
    variable found: std_logic;
  begin
    if (RESET = '1') then
      match_found_array_internal <= (others => '0');
      ERROR_COUNT <= (others => '0');
      REFERENCE_OFFSET <= (others => '0');
      READY <= '0';
    elsif (rising_edge(CLK)) then
      if (INIT = '1') then
        match_found_array_internal <= MATCH_FOUND_ARRAY;
        READY <= '0';
      else
        found := '0';
        for i in 0 to N_PRINCIPAL_AGENT_NUCLEOTIDES-1 loop
          if (match_found_array_internal(i) = '1') then
            found := '1';
            ERROR_COUNT <= ERROR_COUNT_ARRAY(i);
            REFERENCE_OFFSET <= std_logic_vector(to_unsigned(i, REFERENCE_OFFSET'length));
            match_found_array_internal(i) <= '0';
            exit;
          end if;
        end loop;
        READY <= not found;
      end if;
    end if;
  end process;
  
end architecture serializer_architecture;