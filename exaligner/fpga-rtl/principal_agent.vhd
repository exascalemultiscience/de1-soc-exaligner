library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.constants.all;
use work.array_types.all;

entity principal_agent is
  port (
    CLK:                      in std_logic;
    RESET:                    in std_logic;
    RESET_ERROR_COUNT:        in std_logic;
    SHIFT:                    in std_logic;
    SHORT_READ_NUCLEOTIDE:    in std_logic_vector(1 downto 0);
    REFERENCE_NUCLEOTIDE_IN:  in std_logic_vector(1 downto 0);
    REFERENCE_NUCLEOTIDE_OUT: out std_logic_vector(1 downto 0);
    MATCH_FOUND_ARRAY:        out std_logic_vector(N_PRINCIPAL_AGENT_NUCLEOTIDES-1 downto 0);
    ERROR_COUNT_ARRAY:        out ErrorCountArray -- negation of value means: how many extra errors it could contain and still be accepted
  );
end principal_agent;

architecture principal_agent_architecture of principal_agent is
  signal reference_nucleotides: std_logic_vector(2*N_PRINCIPAL_AGENT_NUCLEOTIDES-1 downto 0);
  signal error_count:           ErrorCountArray;
  signal shift_started:         std_logic;
  signal debug_error_count0:    std_logic_vector(ERROR_COUNT_WIDTH-1 downto 0);
  signal debug_error_count1:    std_logic_vector(ERROR_COUNT_WIDTH-1 downto 0);
  signal debug_error_count5:    std_logic_vector(ERROR_COUNT_WIDTH-1 downto 0);
begin
  process (CLK, RESET) is
  begin
    if (RESET = '1') then
      reference_nucleotides <= (others => '0');
      error_count <= (others => (others => '0'));
      shift_started <= '0';
    elsif (rising_edge(CLK)) then
      if (RESET_ERROR_COUNT = '1') then
        error_count <= (others => std_logic_vector(to_signed(ERROR_COUNT_RESET_VALUE, ERROR_COUNT_WIDTH)));
      end if;
      if (SHIFT = '1') then
        shift_started <= '1';
        REFERENCE_NUCLEOTIDE_OUT <= reference_nucleotides(1 downto 0);
      end if;
      if (shift_started = '1') then
        shift_started <= '0';
        for i in 0 to N_PRINCIPAL_AGENT_NUCLEOTIDES-1 loop
          if (SHORT_READ_NUCLEOTIDE(1 downto 0) /= reference_nucleotides(2*i+1 downto 2*i)) then
            error_count(i) <= std_logic_vector(signed(error_count(i)) + 1);
          end if;
        end loop;
        reference_nucleotides <= REFERENCE_NUCLEOTIDE_IN & reference_nucleotides(2*N_PRINCIPAL_AGENT_NUCLEOTIDES-1 downto 2);
      end if;
    end if;
  end process;
  
  process (RESET, error_count) is
  begin
    if (RESET = '1') then
      MATCH_FOUND_ARRAY <= (others => '0');
    else
      for i in 0 to N_PRINCIPAL_AGENT_NUCLEOTIDES-1 loop
        if error_count(i)(ERROR_COUNT_WIDTH-1) = '1' then -- if negative
          MATCH_FOUND_ARRAY(i) <= '1';
        else
          MATCH_FOUND_ARRAY(i) <= '0';
        end if;
      end loop;
    end if;
  end process;
  
  debug_error_count0 <= error_count(0);
  debug_error_count1 <= error_count(1);
  debug_error_count5 <= error_count(5);
  
  ERROR_COUNT_ARRAY <= error_count;
  
end architecture principal_agent_architecture;