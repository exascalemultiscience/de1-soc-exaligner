library ieee;
use ieee.std_logic_1164.all;
use work.constants.all;

package array_types is
  type ErrorCountArray is array (N_PRINCIPAL_AGENT_NUCLEOTIDES-1 downto 0) of std_logic_vector(ERROR_COUNT_WIDTH-1 downto 0);
end package;