#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "bitarray.h"
#include "assert.h"

#include "utils.h"

void pushAlignment(AlignmentData** array, size_t* size, size_t* capacity, AlignmentData alignment) {
    if (*size + 1 > *capacity) {
        *capacity *= 2;
        *array = (AlignmentData*) realloc (*array, *capacity * sizeof(AlignmentData));
    }
    copyAlignment(&(*array)[*size], alignment);
    ++(*size);
}

void copyAlignment(AlignmentData* destination, AlignmentData source) {
    destination->valid = source.valid;
    destination->forward = source.forward;
    destination->position = source.position;
    destination->errorCount = source.errorCount;
}

double log2(double x) {
    return log10(x) / log10(2);
}

ErrorCode readSequenceNameFromFile(FILE* file, char* output) {
    ASSERT(output != NULL && file != NULL);
    if (output == NULL || file == NULL)
        return ERROR;
    
    // maximum READ_BUFFER_SIZE-1 characters
    char buf[READ_BUFFER_SIZE];
    fgets (buf, sizeof(buf), file);
    if (ferror(file))
        return ERROR;
    if (feof(file))
        return EOF_ERROR;
    
    size_t nameLength = strlen(buf);
    if (nameLength < 2)
        return ERROR;
    
    // erase trailing new line
    if (buf[nameLength-1] == '\n')
        buf[nameLength-1] = '\0';
    
    // first character is not needed
    ASSERT(buf[0] == '>' || buf[0] == '@');
    strcpy(output, buf + 1);
    
    return NO_ERROR;
}

ErrorCode readSegmentFromFastaFile(FILE* file, size_t length, size_t* realLength, char* output) {
    ASSERT(realLength != NULL);
    ASSERT(output != NULL);
    if (output == NULL || realLength == NULL)
        return ERROR;
    
    char c;
    size_t i = 0;
    do {
        c = fgetc(file);
        if (c != '\n') {
            if (! (feof(file) || ferror(file))) {
                output[i] = c;
                ++i;
            }
        }
    } while (!(feof(file) || ferror(file)) && i < length);
    
    *realLength = i;
    if (feof(file)) {
        for (; i < length; ++i)
            output[i] = 'A';
    }
    
    output[length] = '\0';
    
    if (ferror(file))
        return ERROR;
    if (feof(file))
        return EOF_ERROR;
    
    return NO_ERROR;
}

ErrorCode readShortReadFromFastqFile(FILE* file, char* sequence, char* quality, size_t* length) {
    ASSERT(sequence != NULL);
    ASSERT(length != NULL);
    if (sequence == NULL || length == NULL)
        return ERROR;
    
    // sequence
    char buf[READ_BUFFER_SIZE];
    fgets (buf, READ_BUFFER_SIZE, file);
    if (ferror(file))
        return ERROR;
    if (feof(file))
        return EOF_ERROR;
    
    *length = strlen(buf);
    if (buf[*length-1] == '\n')
        --(*length);
    buf[*length] = '\0';
    strcpy(sequence, buf);
    
    // skip next line
    fgets(buf, READ_BUFFER_SIZE, file);
    
    // quality
    fgets (buf, READ_BUFFER_SIZE, file);
    if (ferror(file))
        return ERROR;
    if (feof(file))
        return EOF_ERROR;
    
    size_t qualityLength = strlen(buf);
    if (buf[qualityLength-1] == '\n')
        --qualityLength;
    buf[qualityLength] = '\0';
    if (qualityLength != *length)
        return ERROR;
    strcpy(quality, buf);
    
    if (feof(file))
        return EOF_ERROR;
    
    return NO_ERROR;
}

ErrorCode basePairStringToBitArray(const char* sequence, BitArray** output) {
    ASSERT(output != NULL);
    if (output == NULL)
        return ERROR;
    
    size_t nBasePairs = strlen(sequence);
    BitArray* bitArray = bitArrayAlloc(2 * nBasePairs);
    size_t i;
    for (i = 0; i < nBasePairs; ++i) {
        switch (sequence[i]) {
            case 'A': break;                       // 00
            case 'C': setBit(bitArray, 2 * i);     // 01
                      break;
            case 'G': setBit(bitArray, 2 * i + 1); // 10
                      break;
            case 'T': setBit(bitArray, 2 * i);     // 11
                      setBit(bitArray, 2 * i + 1);
                      break;
            default:  bitArrayFree(bitArray);
                      return ERROR;
        }
    }

    *output = bitArray;
    return NO_ERROR;
}

ErrorCode writeOutputHeaders(FILE* file, const char* referenceName, size_t referenceLength, int argc, char** argv) {
    fprintf(file, "@HD\tVN:1.0\tSO:unsorted\n");
    fprintf(file, "@SQ\tSN:%s\tLN:%zu\n", referenceName, referenceLength);
    fprintf(file, "@PG\tID:Exaligner\tVN:1.0");
    fprintf(file, "\tCL:\"%s", argv[0]);
    size_t i;
    for (i = 1; i < argc; ++i)
        fprintf(file, " %s", argv[i]);
    fprintf(file, "\"\n");
    if (ferror(file))
        return ERROR;
    return NO_ERROR;
}

ErrorCode writeOutputLine(FILE* file,
                          const char* shortReadName,
                          const char* shortReadSequence,
                          const char* shortReadQuality,
                          const char* referenceName,
                          AlignmentData alignment) {
    fprintf(file, "%s\t", shortReadName);
    if (alignment.valid) {
        if (alignment.forward)
            fprintf(file, "%u\t", 0);
        else
            fprintf(file, "%u\t", 16);
    } else {
        fprintf(file, "%u\t", 4);
    }
    fprintf(file, "%s\t", referenceName);
    fprintf(file, "%zu\t", alignment.position + 1);
    fprintf(file, "255\t");
    fprintf(file, "%zuM\t", strlen(shortReadSequence));
    fprintf(file, "*\t");
    fprintf(file, "0\t");
    fprintf(file, "0\t");
    fprintf(file, "%s\t", shortReadSequence);
    fprintf(file, "%s\n", shortReadQuality);
    if (ferror(file))
        return ERROR;
    return NO_ERROR;
}

void reverseComplement(const char* sequence, char* output) {
    size_t sequenceLength = strlen(sequence);
    output[sequenceLength] = '\0';
    size_t i = sequenceLength - 1;
    const char* c = sequence;
    while(*c != '\0') {
        switch (*c) {
            case 'A': output[i] = 'T';
                      break;
            case 'C': output[i] = 'G';
                      break;
            case 'G': output[i] = 'C';
                      break;
            case 'T': output[i] = 'A';
                      break;
            default: ASSERT(FALSE);
        }
        ++c;
        --i;
    }
}