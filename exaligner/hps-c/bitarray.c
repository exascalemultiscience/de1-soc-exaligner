#include <stdlib.h>
#include <stdio.h>

#include "assert.h"
#include "definitions.h"

#include "bitarray.h"

BitArray* bitArrayAlloc(size_t nBits) {
    BitArray* bitArray = (BitArray*)malloc(sizeof(BitArray));
    bitArray->nBits = nBits;
    bitArray->nWords = (nBits / WORD_SIZE + 1);
    bitArray->words = (Word*)calloc(bitArray->nWords, sizeof(Word));
    return bitArray;
}

void bitArrayFree(BitArray* bitArray) {
    free(bitArray->words);
    free(bitArray);
}

inline int bIndex(size_t b) {
    return b / WORD_SIZE;
}

inline int bOffset(size_t b) {
    return b % WORD_SIZE;
}

void setBit(BitArray* bitArray, size_t b) {
    ASSERT(b <= bitArray->nBits);
    bitArray->words[bIndex(b)] |= 1 << (bOffset(b)); 
}

void clearBit(BitArray* bitArray, size_t b) {
    ASSERT(b <= bitArray->nBits);
    bitArray->words[bIndex(b)] &= ~(1 << (bOffset(b)));
}

Bit getBit(const BitArray* bitArray, size_t b) {
    ASSERT(b <= bitArray->nBits);
    return (bitArray->words[bIndex(b)] & (1 << (bOffset(b)))) > 0 ? 1 : 0;
}

void bitArrayClear(BitArray* bitArray) {
    size_t i;
    for (i = 0; i < bitArray->nBits; ++i)
        clearBit(bitArray, i);
}

void bitArrayDump(const BitArray* bitArray) {
    int i;
    for (i = bitArray->nBits - 1; i >= 0; --i)
        DEBUG_LOG("%d", getBit(bitArray, i));
    DEBUG_LOG("\n");
}