#ifndef VIRTUAL_MEMORY_H
#define VIRTUAL_MEMORY_H

#include "definitions.h"

ErrorCode initExalignerMemory();
ErrorCode closeExalignerMemory();

void exalignerWrite(uint32_t data);
void exalignerReset();
uint32_t exalignerRead();
BOOL exalignerIsEmpty();
BOOL exalignerIsMatchReady();

#endif