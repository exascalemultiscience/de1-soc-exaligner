#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

#include "hwlib.h"
#include "socal/socal.h"
#include "socal/hps.h"
#include "socal/alt_gpio.h"
#include "hps_0.h"

#include "virtualmemory.h"

#define HW_REGS_BASE ( ALT_STM_OFST )
#define HW_REGS_SPAN ( 0x04000000 )
#define HW_REGS_MASK ( HW_REGS_SPAN - 1 )

#define EXALIGNER_WRITE_ADDRESS_OFFSET 0
#define EXALIGNER_RESET_ADDRESS_OFFSET 4
#define EXALIGNER_READ_ADDRESS_OFFSET 8
#define EXALIGNER_IS_EMPTY_ADDRESS_OFFSET 12

static int fd;
static void *virtualBase;
static void *writeAddress;
static void *resetAddress;
static void *readAddress;
static void *isEmptyAddress;

ErrorCode initExalignerMemory() {
    if ((fd = open("/dev/mem", O_RDWR | O_SYNC)) == -1) {
        printf("ERROR: could not open \"/dev/mem\"...\n");
        return ERROR;
    }
    
    virtualBase = mmap(NULL, HW_REGS_SPAN, PROT_READ | PROT_WRITE, MAP_SHARED, fd, HW_REGS_BASE);
    
    if (virtualBase == MAP_FAILED) {
        printf("ERROR: mmap() failed...\n");
        close(fd);
        return ERROR;
    }
    
    void *exalignerBaseAddress = virtualBase + ((unsigned long)(ALT_LWFPGASLVS_OFST + EXALIGNER_0_BASE) & (unsigned long)(HW_REGS_MASK));
    writeAddress = exalignerBaseAddress + EXALIGNER_WRITE_ADDRESS_OFFSET;
    resetAddress = exalignerBaseAddress + EXALIGNER_RESET_ADDRESS_OFFSET;
    readAddress = exalignerBaseAddress + EXALIGNER_READ_ADDRESS_OFFSET;
    isEmptyAddress = exalignerBaseAddress + EXALIGNER_IS_EMPTY_ADDRESS_OFFSET;
    
    return NO_ERROR;
}

ErrorCode closeExalignerMemory() {
    if (munmap(virtualBase, HW_REGS_SPAN) != 0) {
        printf("ERROR: munmap() failed...\n");
        close(fd);
        return ERROR;
    }
    
    close(fd);
    
    return NO_ERROR;
}

void exalignerWrite(uint32_t data) {
    *(uint32_t *)writeAddress = data;
}

void exalignerReset() {
    *(uint32_t *)resetAddress = 1;
}

uint32_t exalignerRead() {
    return *(uint32_t *)readAddress;
}

BOOL exalignerIsEmpty() {
    return *(uint32_t *)isEmptyAddress & 1;
}

BOOL exalignerIsMatchReady() {
    return *(uint32_t *)isEmptyAddress >> 1;
}