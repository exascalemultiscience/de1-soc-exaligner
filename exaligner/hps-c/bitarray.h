#ifndef BIT_ARRAY_H
#define BIT_ARRAY_H

#include <stdint.h>

typedef uint32_t Word;
enum { WORD_SIZE = sizeof(Word) * 8 };

typedef struct {
    Word* words;
    size_t nWords;
    size_t nBits;
} BitArray;

typedef int Bit;

// content initialized to 0
BitArray* bitArrayAlloc(size_t nBits);
void bitArrayFree(BitArray* bitArray);

void setBit(BitArray* bitArray, size_t b);
void clearBit(BitArray* bitArray, size_t b);
Bit getBit(const BitArray* bitArray, size_t b);

void bitArrayClear(BitArray* bitArray);

void bitArrayDump(const BitArray* bitArray);

#endif