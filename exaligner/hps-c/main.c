#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

#include "definitions.h"
#include "assert.h"
#include "utils.h"
#include "virtualmemory.h"

void printUsage(const char* programName)
{
    printf("Usage: %s <FASTA reference file> <FASTQ reads file> <SAM output file> [options...]\n", programName);
    printf("Options:\n");
    printf(" -h, --help\n");
}

int main(int argc, char* argv[]) {
    size_t i;
    for (i = 1; i < argc; ++i)
        if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0)
            break;
    if (i < argc) {
        printUsage(argv[0]);
        return 0;
    }
    
    if (argc < 4) {
        printf("ERROR: no output file provided\n");
        if (argc < 3) {
            printf("ERROR: no reads file provided\n");
            if (argc < 2) {
                printf("ERROR: no reference file provided\n");
            }
        }
        printUsage(argv[0]);
        return 1;
    }
    
    ERROR_COUNT_WIDTH = ceil(log2(MAX_N_SHORT_READ_NUCLEOTIDES)) + 1;;
    REFERENCE_OFFSET_WIDTH = ceil(log2(N_PRINCIPAL_AGENT_NUCLEOTIDES));
    ERROR_COUNT_MASK = (pow(2, ERROR_COUNT_WIDTH) - 1);
    REFERENCE_OFFSET_MASK = (pow(2, REFERENCE_OFFSET_WIDTH) - 1);
    
    AlignmentData* alignments = (AlignmentData*) malloc(sizeof(AlignmentData));
    size_t alignmentsSize = 0;
    size_t alignmentsCapacity = 1;
    
    char* referenceFileName = argv[1];
    char* shortReadsFileName = argv[2];
    char* outputFileName = argv[3];
    
    if (initExalignerMemory() != NO_ERROR)
        return 1;

    ErrorCode referenceFileError = NO_ERROR;
    FILE *referenceFile;
    referenceFile = fopen(referenceFileName, "r");
    
    char referenceSequenceName[READ_BUFFER_SIZE];
    referenceFileError |= readSequenceNameFromFile(referenceFile, referenceSequenceName);
    size_t referenceSequenceLength = 0;

    char endOfLastReferenceSegment[MAX_N_SHORT_READ_NUCLEOTIDES];
    endOfLastReferenceSegment[0] = '\0';
    
    unsigned int chunkCounter = 0;
    while (referenceFileError == NO_ERROR) {
        exalignerReset();
        printf("chunkCounter: %d\n", chunkCounter);
        char referenceSegment[N_PRINCIPAL_AGENT_NUCLEOTIDES+1];
        size_t referenceSegmentRealLength = 0;
        if (endOfLastReferenceSegment[0] == '\0') {
            referenceFileError |= readSegmentFromFastaFile(referenceFile, N_PRINCIPAL_AGENT_NUCLEOTIDES, &referenceSegmentRealLength, referenceSegment);
        } else {
            strcpy(referenceSegment, endOfLastReferenceSegment);
            referenceFileError |= readSegmentFromFastaFile(referenceFile,
                                                           N_PRINCIPAL_AGENT_NUCLEOTIDES - MAX_N_SHORT_READ_NUCLEOTIDES,
                                                           &referenceSegmentRealLength, 
                                                           referenceSegment + MAX_N_SHORT_READ_NUCLEOTIDES);
        }
        ASSERT(strlen(referenceSegment) == N_PRINCIPAL_AGENT_NUCLEOTIDES);
        referenceSequenceLength += referenceSegmentRealLength;
        DEBUG_LOG("reference: %s\n", referenceSegment);
        BitArray* referenceSegmentBitArray;
        basePairStringToBitArray(referenceSegment, &referenceSegmentBitArray);
        bitArrayDump(referenceSegmentBitArray);
        size_t j;
        for (j = 0; j < N_PRINCIPAL_AGENT_NUCLEOTIDES; ++j) {
            uint32_t word = (getBit(referenceSegmentBitArray, 2*j+1) << 1) + getBit(referenceSegmentBitArray, 2*j);
            exalignerWrite(word);
        }
        DEBUG_LOG("\n");
        bitArrayFree(referenceSegmentBitArray);
        
        unsigned int shiftOffset = 0;
        char shortReadName[READ_BUFFER_SIZE];
        char shortRead[MAX_N_SHORT_READ_NUCLEOTIDES+1], shortReadReverseComplement[MAX_N_SHORT_READ_NUCLEOTIDES+1];
        char shortReadQuality[MAX_N_SHORT_READ_NUCLEOTIDES+1];
        size_t shortReadLength;
        FILE* shortReadsFile;
        shortReadsFile = fopen(shortReadsFileName, "r");
        ErrorCode shortReadsFileError = NO_ERROR;
        
        unsigned int shortReadCounter = 0;
        while (shortReadsFileError == NO_ERROR) {
            shortReadsFileError |= readSequenceNameFromFile(shortReadsFile, shortReadName);
            shortReadsFileError |= readShortReadFromFastqFile(shortReadsFile, shortRead, shortReadQuality, &shortReadLength);
            if (shortReadLength > MAX_N_SHORT_READ_NUCLEOTIDES)
                continue;
            
            // forward
            exalignerWrite(shortReadLength);
            // placed here in order to utilize idle clock cycles
            if (shortReadCounter >= alignmentsSize) {
                AlignmentData alignment = {.valid = FALSE, .forward = FALSE, .position = 0, .errorCount = 0};
                pushAlignment(&alignments, &alignmentsSize, &alignmentsCapacity, alignment);
            }
            BitArray* shortReadBitArray;
            basePairStringToBitArray(shortRead, &shortReadBitArray);
            DEBUG_LOG("shortRead: %s\n", shortRead);
            bitArrayDump(shortReadBitArray);
            size_t j;
            for (j = 0; j < shortReadLength; ++j) {
                uint32_t word = (getBit(shortReadBitArray, 2*j+1) << 1) + getBit(shortReadBitArray, 2*j);
                exalignerWrite(word);
            }
            while (! exalignerIsMatchReady()) ;
            while (! exalignerIsEmpty()) {
                uint32_t result = exalignerRead();
                DEBUG_LOG("result: %d\n", result);
                unsigned int referenceOffset = result & REFERENCE_OFFSET_MASK;
                unsigned int chunkOffset = (referenceOffset + shiftOffset) % N_PRINCIPAL_AGENT_NUCLEOTIDES;
                int errorCount = (-1 << ERROR_COUNT_WIDTH) + ((result >> REFERENCE_OFFSET_WIDTH) & ERROR_COUNT_MASK);
                errorCount = -errorCount;
                errorCount = MAX_ERROR_COUNT + 1 - errorCount;
                DEBUG_LOG("shiftOffset: %d\n", shiftOffset);
                DEBUG_LOG("referenceOffset: %d\n", referenceOffset);
                DEBUG_LOG("chunkOffset: %d\n", chunkOffset);
                DEBUG_LOG("errorCount: %d\n", errorCount);
                if (0 <= chunkOffset && chunkOffset < N_PRINCIPAL_AGENT_NUCLEOTIDES - MAX_N_SHORT_READ_NUCLEOTIDES) {
                    size_t referenceSequenceOffset = chunkCounter * (N_PRINCIPAL_AGENT_NUCLEOTIDES - MAX_N_SHORT_READ_NUCLEOTIDES) + chunkOffset;
                    DEBUG_LOG("referenceSequenceOffset: %d\n", referenceSequenceOffset);
                    if (!alignments[shortReadCounter].valid || (alignments[shortReadCounter].valid && alignments[shortReadCounter].errorCount > errorCount)) {
                        AlignmentData alignment = {.valid = TRUE, .forward = TRUE, .position = referenceSequenceOffset, .errorCount = errorCount};
                        copyAlignment(&(alignments[shortReadCounter]), alignment);
                    }
                } else {
                    DEBUG_LOG("false positive\n");
                }
            }
            shiftOffset = (shiftOffset + shortReadLength) % N_PRINCIPAL_AGENT_NUCLEOTIDES;
            
            // reverse complement
            exalignerWrite(shortReadLength);
            reverseComplement(shortRead, shortReadReverseComplement);
            DEBUG_LOG("shortRead reverse complement: %s\n", shortReadReverseComplement);
            basePairStringToBitArray(shortReadReverseComplement, &shortReadBitArray);
            bitArrayDump(shortReadBitArray);
            for (j = 0; j < shortReadLength; ++j) {
                uint32_t word = (getBit(shortReadBitArray, 2*j+1) << 1) + getBit(shortReadBitArray, 2*j);
                exalignerWrite(word);
            }
            bitArrayFree(shortReadBitArray);
            while (! exalignerIsMatchReady()) ;
            while (! exalignerIsEmpty()) {
                uint32_t result = exalignerRead();
                DEBUG_LOG("result: %d\n", result);
                unsigned int referenceOffset = result & REFERENCE_OFFSET_MASK;
                unsigned int chunkOffset = (referenceOffset + shiftOffset) % N_PRINCIPAL_AGENT_NUCLEOTIDES;
                int errorCount = (-1 << ERROR_COUNT_WIDTH) + ((result >> REFERENCE_OFFSET_WIDTH) & ERROR_COUNT_MASK);
                errorCount = -errorCount;
                errorCount = MAX_ERROR_COUNT + 1 - errorCount;
                DEBUG_LOG("shiftOffset: %d\n", shiftOffset);
                DEBUG_LOG("referenceOffset: %d\n", referenceOffset);
                DEBUG_LOG("chunkOffset: %d\n", chunkOffset);
                DEBUG_LOG("errorCount: %d\n", errorCount);
                if (0 <= chunkOffset && chunkOffset < N_PRINCIPAL_AGENT_NUCLEOTIDES - MAX_N_SHORT_READ_NUCLEOTIDES) {
                    size_t referenceSequenceOffset = chunkCounter * (N_PRINCIPAL_AGENT_NUCLEOTIDES - MAX_N_SHORT_READ_NUCLEOTIDES) + chunkOffset;
                    DEBUG_LOG("referenceSequenceOffset: %d\n", referenceSequenceOffset);
                    if (!alignments[shortReadCounter].valid || (alignments[shortReadCounter].valid && alignments[shortReadCounter].errorCount > errorCount)) {
                        AlignmentData alignment = {.valid = TRUE, .forward = FALSE, .position = referenceSequenceOffset, .errorCount = errorCount};
                        copyAlignment(&(alignments[shortReadCounter]), alignment);
                    }
                } else {
                    DEBUG_LOG("false positive\n");
                }
            }
            shiftOffset = (shiftOffset + shortReadLength) % N_PRINCIPAL_AGENT_NUCLEOTIDES;
            
            ++shortReadCounter;
        }
        
        fclose(shortReadsFile);
        
        strcpy(endOfLastReferenceSegment, referenceSegment + N_PRINCIPAL_AGENT_NUCLEOTIDES - MAX_N_SHORT_READ_NUCLEOTIDES);
        ++chunkCounter;
    }
    
    fclose(referenceFile);

    FILE* outputFile;
    outputFile = fopen(outputFileName, "w");
    ErrorCode outputFileError = NO_ERROR;
    outputFileError |= writeOutputHeaders(outputFile, referenceSequenceName, referenceSequenceLength, argc, argv);

    char shortReadName[READ_BUFFER_SIZE];
    char shortRead[MAX_N_SHORT_READ_NUCLEOTIDES+1];
    char shortReadQuality[MAX_N_SHORT_READ_NUCLEOTIDES+1];
    size_t shortReadLength;
    FILE* shortReadsFile;
    shortReadsFile = fopen(shortReadsFileName, "r");
    ErrorCode shortReadsFileError = NO_ERROR;
    
    unsigned int shortReadCounter = 0;
    while (outputFileError == NO_ERROR && shortReadsFileError == NO_ERROR) {
        shortReadsFileError |= readSequenceNameFromFile(shortReadsFile, shortReadName);
        shortReadsFileError |= readShortReadFromFastqFile(shortReadsFile, shortRead, shortReadQuality, &shortReadLength);
        if (shortReadLength > MAX_N_SHORT_READ_NUCLEOTIDES)
            continue;
        
        outputFileError |= writeOutputLine(outputFile,
                                           shortReadName,
                                           shortRead,
                                           shortReadQuality,
                                           referenceSequenceName,
                                           alignments[shortReadCounter]);
        ++shortReadCounter;
    }
    
    fclose(shortReadsFile);
    fclose(outputFile);
    
    if (closeExalignerMemory() != NO_ERROR)
        return 1;

    return 0;
}
