#ifndef ASSERT_H
#define ASSERT_H

#ifdef NDEBUG
    #define ASSERT(condition) (condition)
#else
    #define ASSERT(condition) \
        if(!(condition)) { \
            printf("ERROR: Assertion failed at %s:%d inside %s. Condition: %s\n", __FILE__, __LINE__, __FUNCTION__, #condition); \
        }
#endif

#endif