#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#define BOOL unsigned int
#define TRUE 1
#define FALSE 0

typedef unsigned int ErrorCode;
#define NO_ERROR 0
#define ERROR 1
#define EOF_ERROR 2

#ifdef UNUSED
#elif defined(__GNUC__)
#define UNUSED(x) UNUSED_ ## x __attribute__((unused))
#elif defined(__LCLINT__)
#define UNUSED(x) /*@unused@*/ x
#else
#define UNUSED(x) x
#endif

#define DEBUG_LOG // printf

#define MAX_N_SHORT_READ_NUCLEOTIDES 50
#define N_PRINCIPAL_AGENT_NUCLEOTIDES 64
#define MAX_ERROR_COUNT 3
#define AVALON_MM_DATA_WIDTH 32
#define READ_BUFFER_SIZE 4096

// initialized in main
extern unsigned int ERROR_COUNT_WIDTH;
extern unsigned int REFERENCE_OFFSET_WIDTH;
extern unsigned int ERROR_COUNT_MASK;
extern unsigned int REFERENCE_OFFSET_MASK;

#endif
