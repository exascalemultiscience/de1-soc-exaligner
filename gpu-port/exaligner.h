#ifndef EXALIGNER_H
#define EXALIGNER_H

#include <stdint.h>

__global__ void checkReferencePositions(const uint32_t *referenceSegment, const uint32_t *shortRead, uint32_t shortReadLength, uint32_t* errorCounts);

#endif