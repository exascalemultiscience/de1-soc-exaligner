#include "exaligner.h"

// TODO
__device__ uint32_t getBase(const uint32_t *array, const uint32_t i)
{
    return (array[i / 16] & (3 << (i % 32))) >> (i % 32);
}

__device__ uint32_t getBit(const uint32_t *array, const uint32_t i)
{
    return (array[i / 32] & (1 << (i % 32))) ? 1 : 0;
}

__global__ void checkReferencePositions(const uint32_t *referenceSegment, const uint32_t *shortRead, uint32_t shortReadLength, uint32_t* errorCounts)
{
    unsigned int i = threadIdx.x + blockDim.x * blockIdx.x;

    uint32_t errorCount = 0;
    size_t j;
    for (j = 0; j < shortReadLength; ++j) {
        //if (getBase(referenceSegment, i + j) != getBase(shortRead, j)) {
        if (getBit(referenceSegment, 2*(i + j)) != getBit(shortRead, 2*j) ||
            getBit(referenceSegment, 2*(i + j) + 1) != getBit(shortRead, 2*j + 1)) {
            ++errorCount;
        }
    }

    errorCounts[i] = errorCount;
}