#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

#include <cuda.h>
#include <cuda_runtime_api.h>

#include "definitions.h"
#include "assert.h"
#include "utils.h"
#include "exaligner.h"

unsigned int ERROR_COUNT_WIDTH = ceil(log2((double)MAX_N_SHORT_READ_NUCLEOTIDES)) + 1;
unsigned int REFERENCE_OFFSET_WIDTH = ceil(log2((double)N_PRINCIPAL_AGENT_NUCLEOTIDES));
unsigned int ERROR_COUNT_MASK = (pow(2, ERROR_COUNT_WIDTH) - 1);
unsigned int REFERENCE_OFFSET_MASK = (pow(2, REFERENCE_OFFSET_WIDTH) - 1);

void printUsage(const char* programName)
{
    printf("Usage: %s <FASTA reference file> <FASTQ reads file> <SAM output file> [options...]\n", programName);
    printf("Options:\n");
    printf(" -h, --help\n");
}

int main(int argc, char* argv[])
{
    size_t i;
    for (i = 1; i < argc; ++i)
        if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0)
            break;
    if (i < argc) {
        printUsage(argv[0]);
        return 0;
    }
    
    if (argc < 4) {
        printf("ERROR: no output file provided\n");
        if (argc < 3) {
            printf("ERROR: no reads file provided\n");
            if (argc < 2) {
                printf("ERROR: no reference file provided\n");
            }
        }
        printUsage(argv[0]);
        return 1;
    }
    
    AlignmentData* alignments = (AlignmentData*) malloc(sizeof(AlignmentData));
    size_t alignmentsSize = 0;
    size_t alignmentsCapacity = 1;
    
    char* referenceFileName = argv[1];
    char* shortReadsFileName = argv[2];
    char* outputFileName = argv[3];

    ErrorCode referenceFileError = NO_ERROR;
    FILE *referenceFile;
    referenceFile = fopen(referenceFileName, "r");
    char referenceSequenceName[READ_BUFFER_SIZE];
    referenceFileError |= readSequenceNameFromFile(referenceFile, referenceSequenceName);
    size_t referenceSequenceLength = 0;

    char endOfLastReferenceSegment[MAX_N_SHORT_READ_NUCLEOTIDES];
    endOfLastReferenceSegment[0] = '\0';

    uint32_t* referenceWords;
    size_t nReferenceBytes = N_PRINCIPAL_AGENT_NUCLEOTIDES / 4;
    cudaError_t cudaError = cudaMalloc((void**)&referenceWords, nReferenceBytes);
    if(cudaError != cudaSuccess) {
        printf("ERROR: couldn't allocate memory\n");
        return 1;
    }
    uint32_t* shortReadWords;
    cudaMalloc((void**)&shortReadWords, ceil((float)MAX_N_SHORT_READ_NUCLEOTIDES / 4));
    if(shortReadWords == 0) {
        printf("ERROR: couldn't allocate memory\n");
        return 1;
    }
    uint32_t *errorCounts = 0;
    uint32_t *errorCountsHost = 0;
    size_t nErrorCounters = (N_PRINCIPAL_AGENT_NUCLEOTIDES - MAX_N_SHORT_READ_NUCLEOTIDES);
    errorCountsHost = (uint32_t*)malloc(nErrorCounters * sizeof(uint32_t));
    cudaMalloc((void**)&errorCounts, nErrorCounters * sizeof(uint32_t));
    if(errorCounts == 0) {
        printf("ERROR: couldn't allocate memory\n");
        return 1;
    }

    unsigned int chunkCounter = 0;
    while (referenceFileError == NO_ERROR) {
        printf("chunkCounter: %d\n", chunkCounter);
        char referenceSegment[N_PRINCIPAL_AGENT_NUCLEOTIDES+1];
        size_t referenceSegmentRealLength = 0;
        if (endOfLastReferenceSegment[0] == '\0') {
            referenceFileError |= readSegmentFromFastaFile(referenceFile, N_PRINCIPAL_AGENT_NUCLEOTIDES, &referenceSegmentRealLength, referenceSegment);
        } else {
            strcpy(referenceSegment, endOfLastReferenceSegment);
            referenceFileError |= readSegmentFromFastaFile(referenceFile,
                                                           N_PRINCIPAL_AGENT_NUCLEOTIDES - MAX_N_SHORT_READ_NUCLEOTIDES,
                                                           &referenceSegmentRealLength, 
                                                           referenceSegment + MAX_N_SHORT_READ_NUCLEOTIDES);
        }
        ASSERT(strlen(referenceSegment) == N_PRINCIPAL_AGENT_NUCLEOTIDES);
        referenceSequenceLength += referenceSegmentRealLength;
        DEBUG_LOG("reference: %s\n", referenceSegment);
        BitArray* referenceSegmentBitArray;
        basePairStringToBitArray(referenceSegment, &referenceSegmentBitArray);
        bitArrayDump(referenceSegmentBitArray);
        DEBUG_LOG("\n");
        
        cudaMemcpy(referenceWords, referenceSegmentBitArray->words, nReferenceBytes, cudaMemcpyHostToDevice);

        unsigned int shiftOffset = 0;
        char shortReadName[READ_BUFFER_SIZE];
        char shortRead[MAX_N_SHORT_READ_NUCLEOTIDES+1], shortReadReverseComplement[MAX_N_SHORT_READ_NUCLEOTIDES+1];
        char shortReadQuality[MAX_N_SHORT_READ_NUCLEOTIDES+1];
        size_t shortReadLength;
        FILE* shortReadsFile;
        shortReadsFile = fopen(shortReadsFileName, "r");
        ErrorCode shortReadsFileError = NO_ERROR;
        
        unsigned int shortReadCounter = 0;
        while (shortReadsFileError == NO_ERROR) {
            shortReadsFileError |= readSequenceNameFromFile(shortReadsFile, shortReadName);
            shortReadsFileError |= readShortReadFromFastqFile(shortReadsFile, shortRead, shortReadQuality, &shortReadLength);
            if (shortReadLength > MAX_N_SHORT_READ_NUCLEOTIDES)
                continue;

            if (shortReadCounter >= alignmentsSize) {
                AlignmentData alignment = {.valid = FALSE, .forward = FALSE, .position = 0, .errorCount = 0};
                pushAlignment(&alignments, &alignmentsSize, &alignmentsCapacity, alignment);
            }

            // forward
            DEBUG_LOG("shortRead forward: %s\n", shortRead);
            BitArray* shortReadBitArray;
            basePairStringToBitArray(shortRead, &shortReadBitArray);
            bitArrayDump(shortReadBitArray);

            size_t nShortReadBytes = ceil((float)shortReadLength / 4);
            cudaMemcpy(shortReadWords, shortReadBitArray->words, nShortReadBytes, cudaMemcpyHostToDevice);

            // nErrorCounters elvileg nem lesz több mint 1024
            checkReferencePositions<<<ceil((float)nErrorCounters / 32), 32>>>(referenceWords, shortReadWords, shortReadLength, errorCounts);
            cudaMemcpy(errorCountsHost, errorCounts, nErrorCounters * 4, cudaMemcpyDeviceToHost);

            size_t k;
            for (k = 0; k < nErrorCounters; ++k) {
                if (errorCountsHost[k] <= MAX_ERROR_COUNT) {
                    size_t referenceSequenceOffset = chunkCounter * (N_PRINCIPAL_AGENT_NUCLEOTIDES - MAX_N_SHORT_READ_NUCLEOTIDES) + k;
                    DEBUG_LOG("referenceSequenceOffset: %zu\n", referenceSequenceOffset);
                    if (!alignments[shortReadCounter].valid || (alignments[shortReadCounter].valid && alignments[shortReadCounter].errorCount > errorCountsHost[k])) {
                        AlignmentData alignment = {.valid = TRUE, .forward = TRUE, .position = referenceSequenceOffset, .errorCount = errorCountsHost[k]};
                        copyAlignment(&(alignments[shortReadCounter]), alignment);
                    }
                }
            }
            
            // reverse complement
            reverseComplement(shortRead, shortReadReverseComplement);
            DEBUG_LOG("shortRead reverse complement: %s\n", shortReadReverseComplement);
            basePairStringToBitArray(shortReadReverseComplement, &shortReadBitArray);
            bitArrayDump(shortReadBitArray);

            cudaMemcpy(shortReadWords, shortReadBitArray->words, nShortReadBytes, cudaMemcpyHostToDevice);
            bitArrayFree(shortReadBitArray);
            
            // nErrorCounters elvileg nem lesz több mint 1024
            checkReferencePositions<<<ceil((float)nErrorCounters / 32), 32>>>(referenceWords, shortReadWords, shortReadLength, errorCounts);
            cudaMemcpy(errorCountsHost, errorCounts, nErrorCounters * 4, cudaMemcpyDeviceToHost);

            for (k = 0; k < nErrorCounters; ++k) {
                if (errorCountsHost[k] <= MAX_ERROR_COUNT) {
                    size_t referenceSequenceOffset = chunkCounter * (N_PRINCIPAL_AGENT_NUCLEOTIDES - MAX_N_SHORT_READ_NUCLEOTIDES) + k;
                    DEBUG_LOG("referenceSequenceOffset: %zu\n", referenceSequenceOffset);
                    if (!alignments[shortReadCounter].valid || (alignments[shortReadCounter].valid && alignments[shortReadCounter].errorCount > errorCountsHost[k])) {
                        AlignmentData alignment = {.valid = TRUE, .forward = FALSE, .position = referenceSequenceOffset, .errorCount = errorCountsHost[k]};
                        copyAlignment(&(alignments[shortReadCounter]), alignment);
                    }
                }
            }



            shiftOffset = (shiftOffset + shortReadLength) % N_PRINCIPAL_AGENT_NUCLEOTIDES;
            ++shortReadCounter;
        }
        
        fclose(shortReadsFile);
        
        bitArrayFree(referenceSegmentBitArray);
        strcpy(endOfLastReferenceSegment, referenceSegment + N_PRINCIPAL_AGENT_NUCLEOTIDES - MAX_N_SHORT_READ_NUCLEOTIDES);
        ++chunkCounter;
    }
    
    cudaFree((void*)referenceWords);
    cudaFree((void*)errorCounts);
    cudaFree((void*)shortReadWords);

    fclose(referenceFile);

    FILE* outputFile;
    outputFile = fopen(outputFileName, "w");
    ErrorCode outputFileError = NO_ERROR;
    outputFileError |= writeOutputHeaders(outputFile, referenceSequenceName, referenceSequenceLength, argc, argv);

    char shortReadName[READ_BUFFER_SIZE];
    char shortRead[MAX_N_SHORT_READ_NUCLEOTIDES+1];
    char shortReadQuality[MAX_N_SHORT_READ_NUCLEOTIDES+1];
    size_t shortReadLength;
    FILE* shortReadsFile;
    shortReadsFile = fopen(shortReadsFileName, "r");
    ErrorCode shortReadsFileError = NO_ERROR;
    
    unsigned int shortReadCounter = 0;
    while (outputFileError == NO_ERROR && shortReadsFileError == NO_ERROR) {
        shortReadsFileError |= readSequenceNameFromFile(shortReadsFile, shortReadName);
        shortReadsFileError |= readShortReadFromFastqFile(shortReadsFile, shortRead, shortReadQuality, &shortReadLength);
        if (shortReadLength > MAX_N_SHORT_READ_NUCLEOTIDES)
            continue;
        
        outputFileError |= writeOutputLine(outputFile,
                                           shortReadName,
                                           shortRead,
                                           shortReadQuality,
                                           referenceSequenceName,
                                           alignments[shortReadCounter]);
        DEBUG_LOG("%s %u %u %zu %u\n",
                    shortReadName,
                    alignments[shortReadCounter].valid,
                    alignments[shortReadCounter].forward,
                    alignments[shortReadCounter].position,
                    alignments[shortReadCounter].errorCount);
        
        ++shortReadCounter;
    }
    
    fclose(shortReadsFile);
    fclose(outputFile);
    
    return 0;
}
