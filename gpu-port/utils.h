#ifndef SEQUENCE_UTILS_H
#define SEQUENCE_UTILS_H

#include <unistd.h>

#include "definitions.h"
#include "bitarray.h"

typedef struct {
    BOOL valid;
    BOOL forward;
    size_t position;
    unsigned int errorCount;
} AlignmentData;

void pushAlignment(AlignmentData** array, size_t* size, size_t* capacity, AlignmentData alignment);
void copyAlignment(AlignmentData* destination, AlignmentData source);

//double log2(double x);

ErrorCode readSequenceNameFromFile(FILE* file, char* output);
ErrorCode readSegmentFromFastaFile(FILE* file, size_t length, size_t* realLength, char* output);
ErrorCode readShortReadFromFastqFile(FILE* file, char* sequence, char* quality, size_t* length);

ErrorCode basePairStringToBitArray(const char* sequence, BitArray** output);

ErrorCode writeOutputHeaders(FILE* file, const char* referenceName, size_t referenceLength, int argc, char** argv);
ErrorCode writeOutputLine(FILE* file,
                          const char* shortReadName,
                          const char* shortReadSequence,
                          const char* shortReadQuality,
                          const char* referenceName,
                          AlignmentData alignment);

void reverseComplement(const char* sequence, char* output);

#endif